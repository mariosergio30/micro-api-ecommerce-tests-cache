package demo.service;

import demo.entities.OrderEntity;
import demo.model.PaymentOrderDTO;
import demo.repository.order.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class OrderService {


	@Autowired
	private OrderRepository repository;
	
	
	@Value("${app.services.payment.uri}")
	private String servicePaymentURI;

	@Cacheable(value = "getOrdersCache")
	public List<OrderEntity> getOrders() {

		return this.repository.findAll();			
	}

	@Cacheable(value = "getOrderByIdCache")
	public OrderEntity getOrder(Long orderId) {

		return this.repository.findById(orderId).get();			
	}

	@CacheEvict(value = "getOrdersCache", allEntries = true)
	public OrderEntity create(OrderEntity order) {

		return this.repository.save(order);			
	}

	@Caching(evict = {
			@CacheEvict(value = "getOrdersCache", allEntries = true),
			@CacheEvict(value = "getOrderByIdCache", key = "#id")
	})
	public OrderEntity update(OrderEntity order) {

		return this.repository.save(order);			
	}

	@Caching(evict = {
			@CacheEvict(value = "getOrdersCache", allEntries = true),
			@CacheEvict(value = "getOrderByIdCache", key = "#id")
	})
	public boolean delete(Long id) {

		this.repository.deleteById(id);

		return true;			
	}

	
	
	//@Transactional
	public OrderEntity processOrder(OrderEntity order) {
			
		
		/******  create orde *****/
		order.setStatus("SUBMITTED");
		order = this.repository.saveAndFlush(order);		
		
		//OrderEntity order = this.repository.findById(orderPost.getId()).get();
				
		
		
		
		/***** call FINANCE service (SYNCHRONOUSLY) *****/
		
		PaymentOrderDTO requestFinance = new PaymentOrderDTO();
		requestFinance.setOrderId(order.getId());
		requestFinance.setFiscalNumber(order.getCustomer().getFiscalNumber());
		requestFinance.setValue(order.getTotalValue());
						
		RestTemplate restTemplate = new RestTemplate();	
		ResponseEntity<PaymentOrderDTO> responseFinance = restTemplate.postForEntity(servicePaymentURI, requestFinance, PaymentOrderDTO.class);		
		
		
		if (responseFinance.getBody().getAproved()) {
			
			// update order status
			order.setStatus("PAYMENT_APPROVED");
			this.update(order);
			
			// SEND EMAIL AND SMS TO CUSTOMERS			
			System.out.println("Email Payment: Dear " + order.getCustomer().getName() + " Paymment for your order " + order.getCode() + " was processed.");	
		}
		

		
		/****** call LOGISTIC SERVICE service (SYNCHRONOUSLY)  *****/
		/* TO DO 
		 * 
		 * */		
		boolean callLogisticOk = true;
		
		
		if (callLogisticOk) {
			
			// update order status
			order.setStatus("DELIVERY_SCHEDULED");
			this.update(order);
			
			// SEND EMAIL AND SMS TO CUSTOMERS	
			System.out.println("Email Delivery: Dear " + order.getCustomer().getName() + " Your order " + order.getCode() + " was will be delivery before 2022/XX/XX ");
			
		}
		
		
		
		return order;		
	}

		
	



}
