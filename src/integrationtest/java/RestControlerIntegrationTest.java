import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import demo.entities.OrderEntity;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:configuration/integrationTestsContext.xml")
@ActiveProfiles(profiles = {"integration-tests", "development", "liquibase"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(properties = "liquibase.change-log=classpath:liquibase/db.changelog_master.yaml")
public class RestControlerIntegrationTest {

    private static final String ENDPOINT_ADDRESS = "local://api";

    private RestTemplate restTemplate;

    @Before
    public void setup(){
        ObjectMapper mapper = JsonMapper.builder().configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true).build();

        restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0, converter);
    }

    @Test
    public void test02_whenGetOrdersThenReturnAllOrders() {
        JsonNode expected = JsonNodeFactory.instance.objectNode();

        OrderEntity result = restTemplate.getForObject("local://api/orders/1", OrderEntity.class);

        AssertionsForClassTypes.assertThat(result.getId()).isEqualTo(1);
    }

    @Test
    public void test01_whenGetOrdersThenReturnAllOrders() {
        assertThat(200).isEqualTo(200);
    }
}
