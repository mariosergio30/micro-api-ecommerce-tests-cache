INSTRUTIONS:

1- Execute demo.Application (RUN AS Java Application)

2- In a web Browser type http://localhost:8080/ to see BasicControler in action  

3- In a web Browser:
  type http://localhost:8080/api/orders to see RestControler in action
  type http://localhost:8080/swagger-ui.html to see Swagger with RestControler in action
  type http://localhost:8080/h2-console/ to see H2 Console in action (no password is required)


4- Try to import src/main/resource/extras/apiEcommerce.postman_collection.json in postman tool (https://www.postman.com/), in order to test easily POST, PUT and DELETE REST operations

